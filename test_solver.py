from board import Board
from solver import Solver

b = Board(4)
s = Solver(b.get_hplanes())

print("Board:")
print(b)

s.solve()

print("Solution:")
print(s)

print("Solved:", s.is_solved())
print("Recursed", s.recurs, "times")
