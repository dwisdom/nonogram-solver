# Nonogram Solver

Solves nonograms. Also known as picross.

## Quickstart
Using Python 3:
```
git clone <this repo>
cd nonogram-solver
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python test_solver.py
```
This will create a random board and solve it.

