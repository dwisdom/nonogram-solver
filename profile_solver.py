import cProfile

from board import Board
from solver import Solver

def main(outpath):
  b = Board(5)
  s = Solver(b.get_hplanes())
  print(s)
  cProfile.runctx('s.solve()', globals(), locals(), outpath)

if __name__ == '__main__':
  import argparse
  p = argparse.ArgumentParser()
  p.add_argument('outpath', help='path to store the profiling results')
  args = p.parse_args()
  main(args.outpath)
