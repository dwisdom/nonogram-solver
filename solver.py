import numpy as np

from itertools import permutations
from functools import lru_cache

from board import Board

class Solver:
  
  def __init__(self, hplanes):
    self.__hplanes = hplanes

    self.__row_hints = None
    self.__col_hints = None
    self.__cur_r = 0


    self.__clean_hints()

    self.__size = self.__hplanes[0].shape[0]
    self.__solution = Board(self.__size)
    self.__solution.clear_board()

    self.recurs = 0



  # same as @cache but available in python >=3.2
  # instead of python >=3.9
  @lru_cache(maxsize=None)
  def __parse_list(self, l):
    """
    Go through a list and return the groups of consecutive
    filled-in squares

    :param l: A list to parse
    """
    to_return = []
    r = 0
    for i in range(len(l)):
      if l[i]:
        r += 1
      # If we're at the end of a group, write r
      if not l[i] and r != 0:
        to_return.append(r)
        r = 0
      # If we're at the end of the list, write r
      elif i == len(l)-1 and r != 0:
        to_return.append(r)
        r = 0

    return to_return


  def __is_row_legal(self, row):
    """
    Check whether a specified row is a legal solution

    :param row: Int representing which row to check (0-indexed)
    """
    hint_check = self.__row_hints[row]
    # row_check = [self.__solution.is_filled(row, i) for i in range(self.__size)]
    # Try just using a pointer to the row instead of copying it over every time
    # Cast to tuple so that it's hashable and we can cache it
    row_check = tuple(self.__solution.get_row(row))
    # return np.array_equal(hint_check, self.__parse_list(row_check))
    # Try using == instead of casting to numpy arrays
    return hint_check == self.__parse_list(row_check)

  def __is_col_legal(self, col):
    """
    Check whether a specified column is a legal solution

    :param col: Int representing which column to check (0-indexed)
    """
    hint_check = self.__col_hints[col]
    # Cast to tuple so that it's hashable and we can cache it
    col_check = tuple([self.__solution.is_filled(i, col) for i in range(self.__size)])
    # return np.array_equal(hint_check, self.__parse_list(col_check))
    # Try using == instead of casting to numpy arrays
    return hint_check == self.__parse_list(col_check)



  def is_solved(self):
    # Since we are using legal rows to begin with,
    # check the columns first
    for col in range(self.__size):
      if not self.__is_col_legal(col):
        return False

    for row in range(self.__size):
      if not self.__is_row_legal(row):
        return False

    return True


  def __clean_hints(self):
    """
    Takes the ungainly hint planes and turns them into 2 dicts of lists
    so that they look like
    {
      0: [4, 3, 2],
      1: [15, 1, 1],
      .
      .
      .
    }
    """
    if self.__hplanes is None:
      raise ValueError("Solver.__hplanes is None. No hints to clean.")

    # Clean the rows
    self.__row_hints = {}
    for i, row in enumerate(self.__hplanes[0]):
      to_dict = [x for x in row if x != 0]
      self.__row_hints[i] = to_dict

    # Clean the columns
    self.__col_hints = {}
    for i, col in enumerate(self.__hplanes[1]):
      to_dict = [x for x in col if x != 0]
      self.__col_hints[i] = to_dict


  def __gen_legal_rows(self, row):
    """
    Generate all possible legal rows for a given hint and size

    :param row: Int representing which row hint to use
    """
    hint = self.__row_hints[row]
    num_filled = int(np.sum(hint))  # np.sum([]) returns 0.0 for some reason
    num_empty = self.__size - num_filled

    base_t = [True] * num_filled
    base_f = [False] * num_empty
    base = np.concatenate([base_f, base_t], axis=None)

    perms = permutations(base)
    legal = []
    for perm in perms:
      if self.__parse_list(perm) == hint:
        legal.append(perm)
    return legal


  def solve(self, row_idx=0):

    self.recurs += 1

    # If we found the solution, we're done
    # Only check for a solution if we're in the final row
    if row_idx == self.__size:
      if self.is_solved():
        return True
      else:
        return False

    # If we made it to the end without solving, backtrack
    #if row_idx == self.__size:
    #  return False

    legal_rows = self.__gen_legal_rows(row_idx)

    # Check each of the legal rows
    for r in legal_rows:

      # Assign it as the row
      self.__solution.replace_row(row_idx, np.array(r))

      # If we get all the way to the end, it's solved
      if self.solve(row_idx+1):
        return True

    # Otherwise, backtrack
    return False

  def __str__(self):
    return self.__solution.__str__()




  
if __name__ == '__main__':
  b = Board(4)
  print("\nOriginal Board")
  print(b)
  h = b.get_hplanes()
  s = Solver(h)
  s.solve()
  print("\nSolution")
  print(s)
  print('s.is_solved():', s.is_solved())
  print('recursed', s.recurs, 'times')
