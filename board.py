import numpy as np

class Board:

  def __init__(self, size):
    """
    param size: The size of the board in each dimension
    """
    self.__n_dims = 2

    if size < 2:
      raise ValueError("Board must be at least 2x2")
    else:
      self.__size = size
    self.__board_shape = tuple([self.__size] * self.__n_dims)
    self.__board = None
    self.__hplanes = None

    self.__build_random_board()
    self.__build_hplanes()


  def __build_random_board(self):
    """
    Creates a NumPy array to represent the board
    Populates the board with random squares filled in
    False means not filled in
    True means filled in
    """
    self.__board = np.empty(shape=self.__board_shape, dtype=np.bool_)
    for i in range(len(self.__board.flat)):
      # https://numpy.org/doc/stable/reference/generated/numpy.ndarray.flat.html
      self.__board.flat[i] = np.random.randint(0,2)


  def __build_hplanes(self):
    """
    Need two hyperplanes of dimension n-1 and size n
    to hold all of the hints

    Each hint is an array of integers

    For a plane, need two edges of hints
    For a cube, need two planes of hints
    &c.

    Populates the two hyperplanes with the hints 
    """
    # 2 hyperplanes
    # [size] * n-1
    # size to get an array for each hint
    hplane_shape = (2, *self.__board_shape[1:], self.__size)
    self.__hplanes = np.zeros(shape=hplane_shape, dtype=np.int)

    # Scan the rows to generate hints
    r = 0
    for i, row in enumerate(self.__board):
      r=0
      for j, col in enumerate(row):
        if col:
          r += 1
        
        # If the entire row is full, write r as the hint
        if r == self.__size:
          del_arr = np.delete(self.__hplanes[0][i], 0)
          self.__hplanes[0][i] = np.insert(del_arr, -1, r)
          continue
        
        # If the last square is full, write r as the hint
        elif r != 0 and j == self.__size - 1:
          del_arr = np.delete(self.__hplanes[0][i], 0)
          self.__hplanes[0][i] = np.insert(del_arr, -1, r)

        # If the current space is empty but the previous space wasn't,
        # write r as the hint
        elif not col and r != 0:
          # Delete the first 0 and insert the value at the end
          del_arr = np.delete(self.__hplanes[0][i], 0)
          self.__hplanes[0][i] = np.insert(del_arr, -1, r)
          r = 0

    # Scan the columns to generate hints
    r = 0
    for j in range(self.__size):    # col
      r = 0
      for i in range(self.__size): # row
        if self.__board[i][j]:
          r += 1

        # If the entire col is full, write r as the hint
        if r == self.__size:
          del_arr = np.delete(self.__hplanes[1][j], 0)
          self.__hplanes[1][j] = np.insert(del_arr, -1, r)
        
        # If the last square is full, write r as the hint
        elif r != 0 and i == self.__size - 1:
          del_arr = np.delete(self.__hplanes[1][j], 0)
          self.__hplanes[1][j] = np.insert(del_arr, -1, r)

        # If the current space is empty but the previous space wasn't,
        # write r as the hint
        elif not self.__board[i][j] and r != 0:
          del_arr = np.delete(self.__hplanes[1][j], 0)
          self.__hplanes[1][j] = np.insert(del_arr, -1, r)
          r = 0

  
  def clear_board(self):
    """
    Empties the board
    """
    self.__board = np.zeros((self.__size, self.__size), dtype=np.bool_)


  def replace_row(self, row_idx, row):
    """
    Replace a row with one of your own

    :param row_idx: Which row to replace
    :param row: A NumPy array to replace the row with
    """
    self.__board[row_idx] = row

  
  def get_row(self, row_idx):
    """
    Returns a pointer to a row in the Board

    :param row_idx: Which row to get a pointer to
    """
    return self.__board[row_idx]


  def fill_square(self, row, col):
    """
    Fill in the square at the provided coordinates

    :param row: Row of the square to mark
    :param col: Column of the square to mark
    """
    self.__board[row][col] = True


  def empty_square(self, row, col):
    """
    Empty the square at the provided coordinates

    :param row: Row of the square to empty
    :param col: Column of the square to empty    
    """
    self.__board[row][col] = False

  
  def is_filled(self, row, col):
    """
    Checks whether a square is filled in

    :param row: Row of the square to check
    :param col: Column of the square to check
    """
    return self.__board[row][col]


  def get_hplanes(self):
    return self.__hplanes

  
  def get_size(self):
    return self.__size

  def __eq__(self, other):
    return np.array_equal(self.__board, other.__board)
  

  def __repr__(self):
    return self.__board.__repr__()

  def __str__(self):
    to_return = ''
    for i in range(self.__size):
      to_return += '\n'
      for j in range(self.__size):
        if self.__board[i][j]:
          to_return += 'X '
        else:
          to_return += '- '

    return to_return + '\n'

if __name__ == '__main__':
  b = Board(10)
  print(b)
  print(b.get_hplanes())

  b1 = Board(10)
  print(b1)

  print(b == b1)
    
    
    
